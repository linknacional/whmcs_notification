<?php

// MANUAL https://docs.whmcs.com/classes/7.4/WHMCS/Notification/Contracts/NotificationInterface.html#method_getMessage

namespace WHMCS\Module\Notification\Browser;

use WHMCS\Config\Setting;
use WHMCS\Exception;
//use WHMCS\Mail\Template;
use WHMCS\Module\Notification\DescriptionTrait;
use WHMCS\Module\Contracts\NotificationModuleInterface;
use WHMCS\Notification\Contracts\NotificationInterface;

/**
 * Notification module for delivering notifications via browser to whmcs users
 *
 * All notification modules must implement NotificationModuleInterface
 *
 * @copyright Copyright (c) WHMCS Limited 2005-2017
 * @license http://www.whmcs.com/license/ WHMCS Eula
 */
class Browser implements NotificationModuleInterface
{
    use DescriptionTrait;

    /**
     * Constructor
     *
     * Any instance of a notification module should have the display name and
     * logo filename at the ready.  Therefore it is recommend to ensure these
     * values are set during object instantiation.
     *
     * The Email notification module utilizes the DescriptionTrait which
     * provides methods to fulfill this requirement.
     *
     * @see \WHMCS\Module\Notification\DescriptionTrait::setDisplayName()
     * @see \WHMCS\Module\Notification\DescriptionTrait::setLogoFileName()
     */
    public function __construct(){
        $this->setDisplayName('Browser')
            ->setLogoFileName('logo.png');
    }


    public function get_whmcs_customfield_id(){
        $fields = mysql_query("SELECT id, fieldname FROM tblcustomfields WHERE type = 'client';");
        if (!$fields) {
            return array('0' => 'database error');
        }elseif (mysql_num_rows($fields) >= 1) {
            $dropFieldArray = array('0' => 'selecione um campo');
            while ($field = mysql_fetch_assoc($fields)) {
            // the dropdown field type renders a select menu of options
            $dropFieldArray[$field['id']] = $field['fieldname'];
            }
           return $dropFieldArray;
        } else {
            return array('0' => 'nothing to show');
        }
    }

    /**
     * Settings required for module configuration
     *
     * The method should provide a description of common settings required
     * for the notification module to function.
     *
     * For example, if the module connects to a remote messaging service this
     * might be username and password or OAuth token fields required to
     * authenticate to that service.
     *
     * This is used to build a form in the UI.  The values submitted by the
     * admin based on the form will be validated prior to save.
     * @see testConnection()
     *
     * The return value should be an array structured like other WHMCS modules.
     * @link https://developers.whmcs.com/payment-gateways/configuration/
     *
     * For the Email notification module, the module settings are the sender
     * name and email.  Every email notification will use these values.
     * Other email values, like recipients are defined on a per notification
     * basis.
     * @see notifictionSettings()
     *
     * EX.
     * return [
     *     'api_username' => [
     *         'FriendlyName' => 'API Username',
     *         'Type' => 'text',
     *         'Description' => 'Required username to authenticate with message service',
     *     ],
     *     'api_password' => [
     *         'FriendlyName' => 'API Password',
     *         'Type' => 'password',
     *         'Description' => 'Required password to authenticate with message service',
     *     ],
     * ];
     *
     * @return array
     */
    public function settings(){
        return [
            'gcm_sender_id' => [
                'FriendlyName' => 'gcm_sender_id',
                'Type' => 'text',
                'Description' => 'Get it from firebase.',
                'Placeholder' => '',//Setting::getValue('CompanyName'),
            ],
            'apiKey' => [
                'FriendlyName' => 'apiKey',
                'Type' => 'text',
                'Description' => 'Get it from firebase.',
                'Placeholder' => '',//Setting::getValue('CompanyName'),
            ],
            'projectId' => [
                'FriendlyName' => 'projectId',
                'Type' => 'text',
                'Description' => 'Get it from firebase.',
                'Placeholder' => '',//Setting::getValue('Email'),
            ],
            'customFieldUser' => [
                'FriendlyName' => 'CustomField User Browser',
                'Type' => 'dropdown',
                'Options' => $this->get_whmcs_customfield_id(),
                'Description' => 'Select the field of custom field wich save the value of user browser.',
            ],
        ];
    }

    /**
     * Validate settings for notification module
     *
     * This method will be invoked prior to saving any settings via the UI.
     *
     * Leverage this method to verify authentication and/or authorization when
     * the notification service requires a remote connection.
     *
     * For the Email notification module, connectivity details are already
     * defined by the WHMCS core system, and there are no settings which
     * require further validation, so this method will always return TRUE.
     *
     * @param array $settings
     *
     * @return boolean
     */
    public function testConnection($settings){
        return true;
    }

    /**
     * The individual customisable settings for a notification.
     *
     * EX.
     * ['channel' => [
     *     'FriendlyName' => 'Channel',
     *     'Type' => 'dynamic',
     *     'Description' => 'Select the desired channel for notification delivery.',
     *     'Required' => true,
     *     ],
     * ]
     *
     * The "Type" of a setting can be text, textarea, yesno, system and dynamic
     *
     * @see getDynamicField for how to obtain dynamic values
     *
     * For the Email notification module, the notification should be configured
     * to use a email template and one or more recipients.
     *
     * @return array
     */
    public function notificationSettings(){
        return [
            'image' => [
                'FriendlyName' => 'Image Icon',
                'Type' => 'text',
                'Description' => 'Set the URL of image for notification alert XXX por XXX (obter os valores do tamanho do icone',
            ],
            'title' => [
                'FriendlyName' => 'title',
                'Type' => 'text',
                'Description' => 'Define the title fo the notification',
            ],
        ];
    }


    public function log_var($var, $name='', $to_file=false){
        if ($to_file==true) {
            $txt = @fopen('/home/linknac/public_html/cliente/modules/notifications/Browser/debug.txt','a');
            if ($txt){
                fwrite($txt, "-----------------------------------\n");
                fwrite($txt, $name."\n");
                fwrite($txt,  print_r($var, true)."\n");
                fclose($txt);//
            }
        } else {
             echo '<pre><b>'.$name.'</b>'.
                  print_r($var,true).'</pre>';
        }
      }

    /**
     * The option values available for a 'dynamic' Type notification setting
     *
     * @see notificationSettings()
     *
     * EX.
     * if ($fieldName == 'channel') {
     *     return [ 'values' => [
     *         ['id' => 1, 'name' => 'Tech Support', 'description' => 'Channel ID',],
     *         ['id' => 2, 'name' => 'Customer Service', 'description' => 'Channel ID',],
     *     ],];
     * } elseif ($fieldName == 'botname') {
     *     $restClient = $this->factoryHttpClient($settings);
     *     $operators = $restClient->get('/operators');
     *     return ['values' => $operators->toArray()];
     * }
     *
     * For the Email notification module, a list of possible email templates is
     * aggregated.
     *
     * @param string $fieldName Notification setting field name
     * @param array $settings Settings for the module
     *
     * @return array
     */
    public function getDynamicField($fieldName, $settings)
    {
        if ($fieldName == 'emailTemplate') {
            $templates = Template::whereType('notification')->get();
            $values = [];
            foreach ($templates as $template) {
                $values[] = ['id' => $template->id, 'name' => $template->name, 'description' => 'Email Template ID',];
            }
            return [
                'values' => $values,
            ];
        }
        return [];
    }
    /**
     * Deliver notification
     *
     * This method is invoked when rule criteria are met.
     *
     * In this method, you should craft an appropriately formatted message and
     * transmit it to the messaging service.
     *
     * For the Email notification module, an email template instance is created
     * along with a collection of merge field data (aggregated from all three
     * method arguments respectively). Those items are provided to the local
     * API 'sendmail' action, where an email message is generated and delivered.
     *
     * @param NotificationInterface $notification A notification to send
     * @param array $moduleSettings Configured settings of the notification module
     * @param array $notificationSettings Configured notification settings set by the triggered rule
     *
     * @throws Exception on error of sending email
     */
    public function sendNotification(NotificationInterface $notification, $moduleSettings, $notificationSettings){

        $debug = true;
        //$to = explode(',', $notificationSettings['recipients']);
        //$to = array_filter(array_unique($to));
        //if (!$to) {
        //    throw new Exception('No Recipients Found');
        //}
        

        /// GET ID OF TICKET FROM URL
        $urlArray = $this->urlParsed($notification->getUrl());

        /// check action equal view - URL like: /admin/supporttickets.php?action=view&id=9163
        if ($urlArray['action'] == "view11"){
            // DO THINGS HERE

            //// GET DETAILS USER
            $ticketDetails = localAPI(
            'GetTicket',
                [
                    'ticketid' => $urlArray['id']
                ]
            );
            /////
            $ticketDetails['userid'];/// could be 0 if not a registerd customer
            $ticketDetails['email'];

            /////////// FUNCAO CURL PARA ENVIAR
            define('SERVER_API_KEY',$moduleSettings['apiKey']);

            //// HERE SHOULD DO THE LOOP OF browserid
            $tokens = ['cGVVPiVFhHk:APA91bGXYIDmtSbbTKd-3YMbOn5FwsukK0S5n8Y5ofK1uiHcTvr8iUdjntthcPUAs8MJGUicPJBr7lJ1saOeKRRR9ZoHbDVFmXN9GP172_HUuL4TZuE0lCl7kP-jYGrAflnmkgrvs5c-'];

            $header = [
                'Content-Type: application/json',
                'Authorization: key='.SERVER_API_KEY
            ];

            $msg = [
                'title' =>'Testando Notificação',
                'body'  => 'Testando from Modul WHMCS',
                'icon'  =>  'img/icon.png',
                'image' =>  'img/d.png'
            ];

            $payload = [
                'registration_ids'  => $tokens,
                'data'              => $msg
            ];

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
                CURLOPT_POST => true,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($payload),
                CURLOPT_SSL_VERIFYPEER, false,
                CURLOPT_HTTPHEADER => $header
            ));

            $responseCurl = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err){
                $this->log_var("CURL ERROR".print_r($err, true), true);
            }else{
                $this->log_var("CURL SUCESSO".print_r($responseCurl, true), true);
            }
        }
            /// GET userid and email FROM TICKET ( se o ticket for merged o email não é salvo na tabela dos tickets)
                /// IF userid IS 0 
                    /// save the browserid, userid = 0 and email
                /// ELSE save the userid and email

            /// SAVE THE ID BORWSER IN NEW TABLE: id, browserid, userid, email (if is 0 is anonymous(not customer) ), 
            /// GET THE ID OF BROWSER if is empty do not send. Only sendo if have permission.
/*

        $to = "Meu caro";
        $mergeFields = [
            'to' => $to,
            'gcm_sender_id' => $moduleSettings['gcm_sender_id'],
            'apiKey' => $moduleSettings['apiKey'],
            'projectId' => $moduleSettings['projectId'],
            'customFieldUser' => $moduleSettings['customFieldUser'],
            'notification_title' => $notification->getTitle(),
            'notification_url' => $notification->getUrl(),
            'notification_message' => $notification->getMessage(),
            'notification_id' => $urlArray['id'],
            'notification_attributes' => [],
        ];


        ///////// CHECK THE URL TO SEND THE NOTIFICATION.

        



        //$notification->addAtributte('Davi');

        foreach ($notification->getAttributes() as $attribute) {
            $mergeFields['notification_attributes'][] = [
                'label' => $attribute->getLabel(),
                'value' => $attribute->getValue(),
                'url' => $attribute->getUrl(),
                //'id' => $attribute->getId(),
                'style' => $attribute->getStyle(),
                'icon' => $attribute->getIcon(),
            ];
        }
*/
        //if($debug){
            //$this->log_var("LOG VAR",print_r($mergeFields, true), true);
            //$this->log_var("URLARRAY".print_r($urlArray, true), true);
            $this->log_var("TICKETID".print_r($ticketDetails, true), true);
            $this->log_var("CURLSRESPONSE".print_r($responseCurl, true), true);
            
        //}
    }


        /**
     * Extract URL
     *
     * This method is to get values from URL
     *
     *
     * @param $url the url to parse
     *
     * @return array
     */

    public function urlParsed($url){
        $query_str = parse_url($url, PHP_URL_QUERY);
        parse_str($query_str, $query_params);
        return $query_params;
    }

}
