// Initialize Firebase
  var config = {
    apiKey: "AAAA35EyHjg:APA91bHF0f4gX38iLjcLMVtzb3GnmmPC89dqjZYyN7KYPBHHBAaToByaJqgk_su8HV1JIv6l4hsQz4E7rudVVvpWdF7TVDPbSlH046h2MsjiZ3TT9qQ-PlTjvak6djGl7S75h_MqnbB5",
    authDomain: "whmcs-notification.firebaseapp.com",
    databaseURL: "https://whmcs-notification.firebaseio.com",
    projectId: "whmcs-notification",
    storageBucket: "whmcs-notification.appspot.com",
    messagingSenderId: "960213687864"
  };
  firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.requestPermission()
.then(function(){
  console.log('Tem permissao');
  if(isTokenSentToServer){
     console.log('Token enviado para o servidor');
  }else{
    getRegToken(); 
  }

  //return messaging.getToken();
})
.catch(function(err){
  console.log('Erro na requisicao', err);
})
/*
messaging.onMessage(function(payload){
  console.log('messagem:'+payload)
})
*/
function getRegToken(argument){
   // Get Instance ID token. Initially this makes a network call, once retrieved
  // subsequent calls to getToken will return from cache.
  messaging.getToken()
  .then(function(currentToken) {
    if (currentToken) {
      saveToken(currentToken);
      console.log(currentToken);
      setTokenSentToServer(true);
      //updateUIForPushEnabled(currentToken);
      
    } else {
      // Show permission request.
      console.log('No Instance ID token available. Request permission to generate one.');
      // Show permission UI.
      //updateUIForPushPermissionRequired();
      setTokenSentToServer(false);
    }
  }).catch(function(err) {
    console.log('An error occurred while retrieving token. ', err);
    //showToken('Error retrieving Instance ID token. ', err);
    setTokenSentToServer(false);
  });
}

function setTokenSentToServer(sent) {
  window.localStorage.setItem('sentToServer', sent ? 1 : 0);
}
function isTokenSentToServer() {
  return window.localStorage.getItem('sentToServer') === 1;
}

function saveToken(currentToken){
  console.log(currentToken);
  $.ajax({
    url: 'action.php',
    method:'post',
    data:'token='+currentToken
  }).done(function(result){
    console.log(result);
  })
}

var dnperm = document.getElementById('getNotification');
var dntrigger = document.getElementById('dntrigger');

dnperm.addEventListener('click', function(e){
  e.preventDefault();

  if(!window.Notification){
    alert('Desculpe, mas notificações não são suportadas nesse navegador');    
  }else{
    Notification.requestPermission(function(p){
      console.log(p);
    })
  }
})