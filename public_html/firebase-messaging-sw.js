// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-messaging.js');

// Initialize Firebase
  var config = {
    apiKey: "AIzaSyBkD3O9vYAuJWDuRIJ07PrSv-S-yy60t_0",
    authDomain: "whmcs-notification.firebaseapp.com",
    databaseURL: "https://whmcs-notification.firebaseio.com",
    projectId: "whmcs-notification",
    storageBucket: "whmcs-notification.appspot.com",
    messagingSenderId: "960213687864"
  };
  firebase.initializeApp(config);

var messaging = firebase.messaging();


// If you would like to customize notifications that are received in the
// background (Web app is closed or not in browser focus) then you should
// implement this optional method.
// [START background_handler]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  var notificationTitle = 'Background Message Title';
  var notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});
// [END background_handler]